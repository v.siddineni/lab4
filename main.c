




#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <time.h>
#include <sys/time.h>
#include <memory.h>


#define numthreads 4

static const long Num_To_Sort = 100000;
//static const long Num_To_Sort = 1000;


void quicksort_s(int *a,long left,long right)
{
    if(left>=right)
        return ;
    long i=left;
    long j=right;
    int key=a[i];
    while(i<j)
    {
        while(i<j&&key<=a[j])
            j--;
        a[i]=a[j];
        while(i<j&&key>=a[i])
            i++;
        a[j]=a[i];
    }
    a[i]=key;
    quicksort_s(a,left,i-1);
    quicksort_s(a,i+1,right);

}



// Sequential version of your sort
// If you're implementing the PSRS algorithm, you may ignore this section

void sort_s(int *arr) {
    quicksort_s(arr,0,Num_To_Sort);
}


int* lt;
int* gt;
//parallel partion array
long partition_p(int * a, long p, long r)
{
    lt=(int*)malloc((r-p)*sizeof(int));
    //int lt[r-p];
    //int gt[r-p];
    gt=(int*)malloc((r-p)*sizeof(int));
    long i;
    long j;
    long key = a[r];
    long lt_n = 0;
    long gt_n = 0;

#pragma omp parallel for
    for(i = p; i < r; i++){
        if(a[i] < a[r]){
            lt[lt_n++] = a[i];
        }else{
            gt[gt_n++] = a[i];
        }
    }

    for(i = 0; i < lt_n; i++){
        a[p + i] = lt[i];
    }

    a[p + lt_n] = key;

    for(j = 0; j < gt_n; j++){
        a[p + lt_n + j + 1] = gt[j];
    }

    return p + lt_n;
}

//paralle quicksort
void quicksort_p(int * a, long p, long r)
{
    long div;

    if(p < r){
        div = partition_p(a, p, r);
#pragma omp parallel sections
        {
#pragma omp section
            quicksort_p(a, p, div - 1);
#pragma omp section
            quicksort_p(a, div + 1, r);

        }
    }
}



// Parallel version of your sort
void sort_p(int *arr) {
    quicksort_p(arr,0,Num_To_Sort);
}


int main(int argc, const char **argv) {
    int *arr_s = malloc(sizeof(int) * Num_To_Sort);
    long chunk_size = Num_To_Sort / omp_get_max_threads();

#pragma omp parallel num_threads(omp_get_max_threads())
    {
        int p = omp_get_thread_num();
        unsigned int seed = (unsigned int) time(NULL) + (unsigned int) p;
        long chunk_start = p * chunk_size;
        long chunk_end = chunk_start + chunk_size;
        for (long i = chunk_start; i < chunk_end; i++) {
            arr_s[i] = rand_r(&seed);
        }
    }

    // Copy the array so that the sorting function can operate on it directly.
    // Note that this doubles the memory usage.
    // You may wish to test with slightly smaller arrays if you're running out of memory.
    int *arr_p = malloc(sizeof(int) * Num_To_Sort);
    memcpy(arr_p, arr_s, sizeof(int) * Num_To_Sort);

    struct timeval start, end;

    printf("Timing sequential...\n");
    gettimeofday(&start, NULL);
    sort_s(arr_s);
    gettimeofday(&end, NULL);
    printf("Took %f seconds\n\n", end.tv_sec - start.tv_sec + (double) (end.tv_usec - start.tv_usec) / 1000000);

    free(arr_s);

    printf("Timing parallel...\n");
    gettimeofday(&start, NULL);
    sort_p(arr_p);
    gettimeofday(&end, NULL);
    printf("Took %f seconds\n\n", end.tv_sec - start.tv_sec + (double) (end.tv_usec - start.tv_usec) / 1000000);

    free(arr_p);

    return 0;
}


///citations//

https://en.wikipedia.org/wiki/Quicksort

https://www.interviewbit.com/tutorial/quicksort-algorithm/

https://runestone.academy/runestone/books/published/pythonds/SortSearch/TheQuickSort.html

https://parcomp.wordpress.com/2017/02/26/parallel-merge-sort/

https://www.drdobbs.com/parallel/parallel-in-place-merge-sort/240169094

https://codereview.stackexchange.com/questions/216746/c-parallel-merge-sort

https://www.youtube.com/watch?v=7h1s2SojIRw

https://www.youtube.com/watch?v=Y2UpnCnMB-s

https://www.youtube.com/watch?v=TzeBrDU-JaY

// I used openMP statements by using text book //

